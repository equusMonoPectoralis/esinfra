resource "aws_s3_bucket" "b" {
  bucket = "equus.boubekri.parts"
  acl    = "public-read"
  policy = file("policy.json")
  
  #checkov:skip=CKV_AWS_20:The bucket is a public static content host

  website {
    index_document = "index.html"
    error_document = "index.html"
  }
}