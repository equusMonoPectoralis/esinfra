variable "record_name" {
    type = string
    default = "equus"
}

variable "dns_zone" {
    type = string
    default = "boubekri.parts"
}
